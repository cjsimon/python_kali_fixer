import setuptools

with open("README.md", "r") as fh:
  long_description = fh.read()

setuptools.setup(
  name='kali-fixer',
  version='0.1',
  scripts=['kali_fixer'],
  author='cody simon',
  author_email='codyjsimon1@gmail.com',
  description='fixes packages on kali light',
  long_description=long_description,
  long_description_content_type="text/markdown",
  url='',
  packages=setuptools.find_packages(),
  classifiers=[
    "Programming Language :: Python :: 3",
    "License :: OSE Approved :: MIT License",
    "Operating System :: OS Independent",
  ]

)
